from django.shortcuts import render
from django.http import HttpResponse
from django.views import View
from django.http.response import JsonResponse
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt
# Create your views here.
from django.views.decorators.csrf import csrf_exempt
from braces.views import CsrfExemptMixin
import json
from django.contrib.auth import login
from django.contrib.auth import authenticate
from django.contrib.auth.models import User


class SignUpRoute(CsrfExemptMixin, View):
	authentication_classes = []

	# def get(self, request):
		# print(request.GET["valores"])
		# return JsonResponse(data=response_json, safe=False)

	def post(self, request):
		#para pegar da URL, pedir por request.GET["nomevar"]
		# valor = request.GET["valor"]
		# print(request.GET["valor"])

		#convertendo o body para json
		body_unicode = request.body.decode('utf-8')
		body = json.loads(body_unicode)

		#pegando cada valor do body
		username = body['user']
		email = body['email']
		password = body['password']
		repassword = body['repassword']

		#verificando senhas iguais
		if password == repassword:
			# cadastrando
			user = User.objects.create_user(username, email, password)
			user.save()

			response_json = {
				"message":"Usuário cadastrado com sucesso."
			}

			return JsonResponse(data=response_json, safe=False)
		else:
			response_json = {
				"message":"As senhas não coincidem"
			}
			return JsonResponse(data=response_json, safe=False)

		response_json = {
            "message":"ocorreu um erro interno"
        }

        #exibindo resposta do json
		return JsonResponse(data=response_json, safe=False)

class WelcomeRoute(CsrfExemptMixin, View):
	authentication_classes = []

	def get(self, request):
		name = request.GET["user"]

		response_json = {
			"message":"Bem vindo a nosso site, "+name
		}
		return JsonResponse(data=response_json, safe=False)

class LoginRoute(CsrfExemptMixin, View):
	authentication_classes = []

	def post(self, request):
		#convertendo o body para json
		body_unicode = request.body.decode('utf-8')
		body = json.loads(body_unicode)

		#pegando cada valor do body
		user = body['user']
		password = body['password']

		####aplicar procedimento de login####	

		if(authenticate(username=user, password=password)):
	        ###login efetuado####
			response_json = {
			    "message": "LOGIN efetuado!"
			}
		else:
			###login não efetuado####
			response_json = {
			    "message": "Falha ao efetuar login!"
			}

        #exibindo resposta do json
		return JsonResponse(data=response_json, safe=False)


