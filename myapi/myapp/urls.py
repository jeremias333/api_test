from django.urls import path

import myapp.views as view
from rest_framework_jwt.views import obtain_jwt_token

app_name = 'myapp'

urlpatterns = [
    path('signup', view.SignUpRoute.as_view(), name='signup'),
    path('welcome', view.WelcomeRoute.as_view(), name='welcome'),
    path('login', view.LoginRoute.as_view(), name='login'),
    path('token-auth', obtain_jwt_token),
]