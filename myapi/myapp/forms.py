from django import forms

from django.contrib.auth.forms import UserCreationForm
from accounts.models import User


class CreateUserForm(UserCreationForm):

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].label = 'Username'
        self.fields['email'].label = 'E-mail'
        self.fields['password1'].label = 'Password'
        self.fields['password2'].label = 'Confirm Password'

class SSOForm(forms.Form):

    user_id = forms.CharField()
    user_name = forms.CharField()
    user_email = forms.EmailField()